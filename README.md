# FireHack Scripts

This is a collection of scripts for FireHack.

## License

All scripts are licensed under the MIT (expat) license unless stated otherwise
in the source of the script.