--[[
  Copyright (c) 2013, drizz

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--]]

WEED_NONE = 0
WEED_WILD = 475
WEED_WILD2 = 512 -- ?
WEED_STUBBORN = 60185

local kScriptHandler = CreateFrame("Frame")
local kWeedType = WEED_NONE

function GetVehicleType()
  local guid = UnitGUID("vehicle")
  local unitId = tonumber(guid:sub(-12, -9), 16)

  return unitId
end

local function VehicleCallback()
  if kWeedType == WEED_WILD or kWeedType == WEED_WILD2 then
    local name = UnitCastingInfo("vehicle")

    if name == "Vine Slam" then
      -- Interrupt Vine Slam.
      CastPetAction(2, "vehicle")
    else
      -- Keep pulling.
      CastPetAction(1, "vehicle")
    end
  elseif kWeedType == WEED_STUBBORN then
    -- Keep pulling.
    CastPetAction(1, "vehicle")
  end
end

-- Reset state.
local function ResetFarmville()
  if IsTimerCallback(VehicleCallback) then
    RemoveTimerCallback(VehicleCallback)
  end

  kWeedType = WEED_NONE
end

local function ScriptHandlerOnEvent(self, event, ...)
  if event == "UNIT_ENTERED_VEHICLE" then
    local unit = ...

    if unit == "player" then
      -- We entered a vehicle.
      local vehicleType = GetVehicleType()

      if vehicleType == WEED_STUBBORN or vehicleType == WEED_WILD or vehicleType == WEED_WILD2 then
        -- We entered a "weed" vehicle.
        kWeedType = vehicleType

        SetTimerCallback(VehicleCallback, 500)
      else
        kWeedType = WEED_NONE
      end
    end
  elseif event == "UNIT_EXITED_VEHICLE" then
    local unit = ...

    if unit == "player" then
      -- Stop auto-casting and stuff.
      ResetFarmville()
    end
  end
end

kScriptHandler:SetScript("OnEvent", ScriptHandlerOnEvent)
kScriptHandler:RegisterEvent("UNIT_ENTERED_VEHICLE")
kScriptHandler:RegisterEvent("UNIT_EXITED_VEHICLE")
